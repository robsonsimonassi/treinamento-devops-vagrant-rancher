#sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock 
sudo rm /var/lib/dpkg/lock


#!/bin/bash

####VAGRANT####

sudo wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb?_ga=2.202741972.433446191.1513560139-1622031488.1511528804

sudo dpkg -i vagrant_2.0.1_x86_64.deb?_ga=2.202741972.433446191.1513560139-1622031488.1511528804

sudo vagrant -v


vagrant box add robsonsimonassi/xenial64Docker

 
####RANCHER######

sudo wget https://releases.rancher.com/cli/v0.6.5/rancher-linux-amd64-v0.6.5.tar.gz

sudo tar -vzxf rancher-linux-amd64-v0.6.5.tar.gz

cd rancher-v0.6.5

sudo mv rancher /usr/bin

cd ..

sudo rm -rf rancher-linux-amd64-v0.6.2.tar.gz

sudo rm -rf rancher-v0.6.2

######### PIP ##########
sudo apt-get install python-pip

sudo pip install --upgrade pip

sudo pip install setuptools

sudo pip install rancher-agent-registration


