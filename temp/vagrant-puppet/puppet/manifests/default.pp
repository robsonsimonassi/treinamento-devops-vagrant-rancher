class utils {

    package { 'iptraf':
        ensure => present,
    }

}

node "rancher" {

  include utils

}